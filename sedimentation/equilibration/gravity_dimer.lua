#!/usr/bin/env luajit
------------------------------------------------------------------------------
-- Equilibration phase of many-dimer simulation.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

-- Check uses of undeclared global variables.
require("strict")

local nm = {
  config = require("nanomotor.config"),
  box = require("nanomotor.box"),
  domain = require("nanomotor.domain"),
  position = require("nanomotor.position"),
  velocity = require("nanomotor.velocity"),
  integrate = require("nanomotor.integrate"),
  observe = require("nanomotor.observe"),
  random = require("nanomotor.random"),
  h5md = require("nanomotor.h5md"),
  trajd = require("nanomotor.trajd"),
  trajs = require("nanomotor.trajs"),
  thermo = require("nanomotor.thermo"),
  species = require("nanomotor.species"),
}

local cl = require("opencl")
local hdf5 = require("hdf5")
local ffi = require("ffi")
local syscall = require("syscall")
local json = require("cjson")

-- Read configuration file.
local args = nm.config({}, arg[1])

-- Seed random number generator.
math.randomseed(args.seed)

-- Create OpenCL context on chosen OpenCL device.
local platform = cl.get_platforms()[args.platform]
print(platform:get_info("version"))
print(platform:get_info("name"))
print(platform:get_info("vendor"))
print(platform:get_info("extensions"))
local device = platform:get_devices()[args.device]
print(device:get_info("name"))
print(device:get_info("vendor"))
print(device:get_info("extensions"))
local context = cl.create_context({device})
local queue = context:create_command_queue(device)

-- Create simulation box with given boundary conditions.
local box = nm.box(args)

-- Allocate zero-filled particle buffers.
local dom = nm.domain(queue, box, args)

-- Place sphere-dimers at random positions and orientations.
do
  local rd, spd, idd = dom.rd, dom.spd, dom.idd
  local L, bond = box.L_global, args.bond
  local dist2_CC = (args.sigma.CC * args.r_cut)^2
  local dist2_CN = (args.sigma.CN * args.r_cut)^2
  local dist2_NN = (args.sigma.NN * args.r_cut)^2
  -- distance the dimer needs to stay away from wall
  local dL = args.wall.r_cut * args.wall.sigma.C + args.wall.dz + bond + args.diameter.C/2
  for i = 0, dom.Nd-1, 2 do ::redo::
    rd[i].x = math.random() * L[1]
    rd[i].y = dL + math.random() * (L[2] - 2*dL)
    rd[i].z = dL + math.random() * (L[3] - 2*dL)
  for j = 0, i-1 do
      local dx = rd[i].x - rd[j].x
      local dy = rd[i].y - rd[j].y
      local dz = rd[i].z - rd[j].z
      dx, dy, dz = box.mindist(dx, dy, dz)
      local d2 = dx*dx + dy*dy + dz*dz
      if d2 < (j%2 == 0 and dist2_CC or dist2_CN) then goto redo end
    end
    local x, y, z = nm.random.sphere()
    x = rd[i].x + bond*x
    y = rd[i].y + bond*y
    z = rd[i].z + bond*z
    rd[i+1].x, rd[i+1].y, rd[i+1].z = box.minimage(x, y, z)
    for j = 0, i-1 do
      local dx = rd[i+1].x - rd[j].x
      local dy = rd[i+1].y - rd[j].y
      local dz = rd[i+1].z - rd[j].z
      dx, dy, dz = box.mindist(dx, dy, dz)
      local d2 = dx*dx + dy*dy + dz*dz
      if d2 < (j%2 == 0 and dist2_CN or dist2_NN) then goto redo end
    end
    spd[i].s0, spd[i+1].s0 = 0, 1
    idd[i], idd[i+1] = i, i+1
  end
end

-- Randomly place solvent particles excluding dimer-sphere volumes.
nm.position.random(dom, args)()
-- Pick solvent velocities from Maxwell-Boltzmann distribution.
nm.velocity.gaussian(dom, args)()

-- Create MPCDMD integrator.
local integrate = nm.integrate(dom, args)

-- Create H5MD output file with temporary filename.
local file = hdf5.create_file(args.output..".tmp")
-- Write H5MD metadata group.
nm.h5md.write_meta(file, args)

-- Serialize simulation parameters to JSON string attribute.
do
  local space = hdf5.create_space("scalar")
  local dtype = hdf5.c_s1:copy()
  local s = json.encode(args)
  dtype:set_size(#s+1)
  local attr = file:create_attribute("parameters", dtype, space)
  attr:write(ffi.cast("const char *", s), dtype)
  space:close()
  dtype:close()
  attr:close()
end

-- Create observables.
local observables = {}
if args.thermo then table.insert(observables, nm.thermo(integrate, file, args.thermo)) end
if args.trajd then table.insert(observables, nm.trajd(integrate, file, args.trajd)) end
if args.trajs then table.insert(observables, nm.trajs(integrate, file, args.trajs)) end
if args.species then table.insert(observables, nm.species(integrate, file, args.species)) end

-- Periodically log step, total energy, and remaining run-time.
do
  local nsteps = args.nsteps
  local interval = 1000
  local self = {step = 0}

  function self.observe()
    local t1 = syscall.clock_gettime("monotonic")
    while self.step <= nsteps do
      coroutine.yield()
      local en = integrate.compute_en()
      local t2 = syscall.clock_gettime("monotonic")
      local t = (t2.time - t1.time) / interval * (nsteps - self.step)
      local u = "s"
      if t >= 60 then t, u = t/60, "m" end
      if t >= 60 then t, u = t/60, "h" end
      io.write("#", self.step, " ", en.en_tot, " ", ("%.1f"):format(t), u, "\n")
      io.flush()
      self.step = self.step + interval
      t1 = t2
    end
  end

  table.insert(observables, self)
end

-- Run simulation.
nm.observe(integrate, observables)

-- Synchronize output files to disk.
file:flush()
assert((syscall.fsync(ffi.cast("int *", file:get_vfd_handle())[0])))
file:close()

-- Rename H5MD output file after successful simulation.
assert((syscall.rename(args.output..".tmp", args.output)))
