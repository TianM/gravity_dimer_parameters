-- Edge lengths of simulation domain.
L = {64, 22.5, 128}
-- Periodic boundary conditions.
periodic = {true, false, false}
-- Number of dimer spheres.
Nd = 2 * 30
-- Diameters of dimer spheres.
diameter = {C = 6.0, N = 6.0}
-- Solvent number density.
rho = 9.0
-- Cutoff radius of potential in units of σ.
r_cut = 2^(1/6)
-- Minimum number of steps between solvent particle sorts.
sort = {interval = 500}
-- Masses of neutrally buoyant dimer spheres.
mass = {C = rho * (math.pi/6) * diameter.C^3, N = rho * (math.pi/6) * diameter.N^3}
-- Number of solvent particles excluding volume of dimer spheres.
Ns = math.floor(rho*L[1]*L[2]*L[3] - (Nd/2)*(mass.C + mass.N))
-- Distance of dimer spheres.
bond = (diameter.C + diameter.N)/2 * r_cut
-- Potential well depths of dimer spheres with solvent particles.
epsilon = {CA = 1.0, CB = 1.0, NA = 1.0, NB = 0.5, CC = 10.0, CN = 10.0, NN = 10.0}
-- Potential diameters.
sigma = {C = diameter.C/2, N = diameter.N/2, CC = diameter.C + 1.5, CN = (diameter.C+diameter.N)/2 + 1.5, NN = diameter.N + 1.5}
-- Magnitude of gravitational constant along z-direction.
gravc = 1*10^(-5)
-- Lennard-Jones wall potential for dimer spheres.
wall = {epsilon = 10^(-5), sigma = {C = 5, N = 5}, r_cut = 3^(1.0/6), dz = 5}
-- Forward and backward reaction of solvent particles.
reaction = {rate = 1, wall = {reactive = {false, true, false}, dz = 1}}
-- Verlet neighbour list skin.
skin = 1.0
-- Number of placeholders per dimer-sphere neighbour list.
nlist_size = {ds = 5000, dd = 20}
-- Number of bins per dimension for dimer spheres.
nbin = {math.floor(L[1]/2), math.floor(L[2]/2), math.floor(L[3]/2)}
-- Number of placeholders per bin.
bin_size = 8
-- Multi-particle collision dynamics.
mpcd = {interval = 100, bin_size = 50}
-- Integration time-step.
timestep = 0.01
-- Number of steps.
nsteps = 5000000
-- Snapshot program state.
snapshot = {initial = 10000, final = nsteps - 10000, interval = 10000, output = "gravity_dimer-snapshot.h5"}
-- Thermodynamic variables.
thermo = {initial = 0, final = nsteps, interval = 100, datatype = "double"}
-- Dimer trajectory.
trajd = {initial = 0, final = nsteps, interval = 1000, datatype = "float", species = {"C", "N"}, elements = {"position", "image", "velocity", "force", "species", "id"}}
-- Solvent trajectory.
trajs = {initial = nsteps, final = nsteps, interval = nsteps, datatype = "float", species = {"A", "B"}, elements = {"position", "image", "velocity", "force", "species", "id"}}
-- Count solvent species.
species = {initial = 0, final = nsteps, interval = 1000, species = {"A", "B"}}
-- Mean-square displacement of dimer.
msdd = {initial = 0, final = nsteps, interval = 100, blockcount = 5, blocksize = 100, blockshift = 10, separation = 10000}
-- Velocity autocorrelation function of dimer.
vafd = {initial = 0, final = nsteps, interval = 10, blockcount = 3, blocksize = 1000, blockshift = 10, separation = 1000}
-- Velocity autocorrelation function of dimer, normal and parallel component.
vafd_n = {initial = 0, final = nsteps, interval = 10, blockcount = 3, blocksize = 1000, blockshift = 10, separation = 1000}
vafd_p = {initial = 0, final = nsteps, interval = 10, blockcount = 3, blocksize = 1000, blockshift = 10, separation = 1000}
-- Orientation autocorrelation of dimer.
orient = {initial = 0, final = nsteps, interval = 10, blockcount = 3, blocksize = 1000, blockshift = 10, separation = 1000}
-- OpenCL platforms.
platform = 1
-- OpenCL device per platform._rr001
device = 1
-- Random number generator seed.
seed = "0x0e6a439330bbb5e5"
-- H5MD input filename.
input = "../equilibration/gravity_dimer.h5"
-- Read trajectory at step.
step = 15000000
-- H5MD output filename
output = "./gravity_dimer.h5"
-- H5MD file author.
author = {name = "Tina", email = "tina.mitteramskogler@gmail.com"}
-- H5MD file creator.
creator = {name = "gravity_dimer.lua", version = "1.0"}
