#!/usr/bin/env luajit
------------------------------------------------------------------------------
-- Production phase of many-dimer simulation.
-- Copyright © 2013–2015 Peter Colberg.
-- Distributed under the MIT license. (See accompanying file LICENSE.)
------------------------------------------------------------------------------

-- Check uses of undeclared global variables.
require("strict")

local nm = {
  config = require("nanomotor.config"),
  box = require("nanomotor.box"),
  domain = require("nanomotor.domain"),
  snapshot = require("nanomotor.snapshot"),
  integrate = require("nanomotor.integrate"),
  observe = require("nanomotor.observe"),
  h5md = require("nanomotor.h5md"),
  trajd = require("nanomotor.trajd"),
  trajs = require("nanomotor.trajs"),
  thermo = require("nanomotor.thermo"),
  species = require("nanomotor.species"),
  corr = require("nanomotor.correlation"),
  msdd = require("msdd"),
  vafd = require("vafd"),
  vafd_n = require("vafd_n"),
  vafd_p = require("vafd_p"),
  orient = require("orient"),
}

local cl = require("opencl")
local hdf5 = require("hdf5")
local ffi = require("ffi")
local syscall = require("syscall")
local json = require("cjson")

-- Read configuration file.
local args = nm.config({}, arg[1])

-- Seed random number generator.
math.randomseed(args.seed)

-- Create OpenCL context on chosen OpenCL device.
local platform = cl.get_platforms()[args.platform]
print(platform:get_info("version"))
print(platform:get_info("name"))
print(platform:get_info("vendor"))
print(platform:get_info("extensions"))
local device = platform:get_devices()[args.device]
print(device:get_info("name"))
print(device:get_info("vendor"))
print(device:get_info("extensions"))
local context = cl.create_context({device})
local queue = context:create_command_queue(device)

-- Create simulation box with given boundary conditions.
local box = nm.box(args)

-- Allocate zero-filled particle buffers.
local dom = nm.domain(queue, box, args)

-- Read particle coordinates from H5MD input file.
local file = hdf5.open_file(args.input)
do
  local group = file:open_group("particles/dimer")
  local obs_rd = nm.h5md.open_observable(group, "position")
  local obs_vd = nm.h5md.open_observable(group, "velocity")
  local obs_spd = nm.h5md.open_observable(group, "species")
  local obs_idd = nm.h5md.open_observable(group, "id")
  group:close()
  local filespace_rd = obs_rd:seek(args.step)
  local filespace_vd = obs_vd:seek(args.step)
  local filespace_spd = obs_spd:seek(args.step)
  local filespace_idd = obs_idd:seek(args.step)
  local Nd = filespace_idd:get_simple_extent_dims()[2]
  assert(Nd == dom.Nd)
  filespace_rd:select_hyperslab("set", {0, 0, 0}, nil, {1, dom.Nd, 3})
  filespace_vd:select_hyperslab("set", {0, 0, 0}, nil, {1, dom.Nd, 3})
  filespace_spd:select_hyperslab("set", {0, 0}, nil, {1, dom.Nd})
  filespace_idd:select_hyperslab("set", {0, 0}, nil, {1, dom.Nd})
  local memspace_rd = hdf5.create_simple_space({dom.Nd, 4})
  local memspace_vd = hdf5.create_simple_space({dom.Nd, 4})
  local memspace_spd = hdf5.create_simple_space({dom.Nd, 4})
  local memspace_idd = hdf5.create_simple_space({dom.Nd})
  memspace_rd:select_hyperslab("set", {0, 0}, nil, {dom.Nd, 3})
  memspace_vd:select_hyperslab("set", {0, 0}, nil, {dom.Nd, 3})
  memspace_spd:select_hyperslab("set", {0, 0}, nil, {dom.Nd, 1})
  local dtype_spd = hdf5.int8:enum_create()
  dtype_spd:enum_insert("C", ffi.new("cl_char[1]", 0))
  dtype_spd:enum_insert("N", ffi.new("cl_char[1]", 1))
  obs_rd.value:read(dom.rd, hdf5.double, memspace_rd, filespace_rd)
  obs_vd.value:read(dom.vd, hdf5.double, memspace_vd, filespace_vd)
  obs_spd.value:read(dom.spd, dtype_spd, memspace_spd, filespace_spd)
  obs_idd.value:read(dom.idd, hdf5.int32, memspace_idd, filespace_idd)
  filespace_rd:close()
  filespace_vd:close()
  filespace_spd:close()
  filespace_idd:close()
  memspace_rd:close()
  memspace_vd:close()
  memspace_spd:close()
  memspace_idd:close()
  dtype_spd:close()
  obs_rd:close()
  obs_vd:close()
  obs_spd:close()
  obs_idd:close()
end
do
  local group = file:open_group("particles/solvent")
  local obs_rs = nm.h5md.open_observable(group, "position")
  local obs_vs = nm.h5md.open_observable(group, "velocity")
  local obs_sps = nm.h5md.open_observable(group, "species")
  local obs_ids = nm.h5md.open_observable(group, "id")
  group:close()
  local filespace_rs = obs_rs:seek(args.step)
  local filespace_vs = obs_vs:seek(args.step)
  local filespace_sps = obs_sps:seek(args.step)
  local filespace_ids = obs_ids:seek(args.step)
  local Ns = filespace_ids:get_simple_extent_dims()[2]
  assert(Ns == dom.Ns)
  filespace_rs:select_hyperslab("set", {0, 0, 0}, nil, {1, dom.Ns, 3})
  filespace_vs:select_hyperslab("set", {0, 0, 0}, nil, {1, dom.Ns, 3})
  filespace_sps:select_hyperslab("set", {0, 0}, nil, {1, dom.Ns})
  filespace_ids:select_hyperslab("set", {0, 0}, nil, {1, dom.Ns})
  local memspace_rs = hdf5.create_simple_space({dom.Ns, 4})
  local memspace_vs = hdf5.create_simple_space({dom.Ns, 4})
  local memspace_sps = hdf5.create_simple_space({dom.Ns, 4})
  local memspace_ids = hdf5.create_simple_space({dom.Ns})
  memspace_rs:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
  memspace_vs:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 3})
  memspace_sps:select_hyperslab("set", {0, 0}, nil, {dom.Ns, 1})
  local dtype_sps = hdf5.int8:enum_create()
  dtype_sps:enum_insert("A", ffi.new("cl_char[1]", 0))
  dtype_sps:enum_insert("B", ffi.new("cl_char[1]", 1))
  obs_rs.value:read(dom.rs, hdf5.double, memspace_rs, filespace_rs)
  obs_vs.value:read(dom.vs, hdf5.double, memspace_vs, filespace_vs)
  obs_sps.value:read(dom.sps, dtype_sps, memspace_sps, filespace_sps)
  obs_ids.value:read(dom.ids, hdf5.int32, memspace_ids, filespace_ids)
  filespace_rs:close()
  filespace_vs:close()
  filespace_sps:close()
  filespace_ids:close()
  memspace_rs:close()
  memspace_vs:close()
  memspace_sps:close()
  memspace_ids:close()
  dtype_sps:close()
  obs_rs:close()
  obs_vs:close()
  obs_sps:close()
  obs_ids:close()
  queue:enqueue_write_buffer(dom.d_rs, true, dom.rs)
  queue:enqueue_write_buffer(dom.d_vs, true, dom.vs)
  queue:enqueue_write_buffer(dom.d_sps, true, dom.sps)
  queue:enqueue_write_buffer(dom.d_ids, true, dom.ids)
end
file:close()

-- Create MPCDMD integrator.
local integrate = nm.integrate(dom, args)

-- Create H5MD output file with temporary filename.
local file = hdf5.create_file(args.output..".tmp")
-- Write H5MD metadata group.
nm.h5md.write_meta(file, args)

-- Serialize simulation parameters to JSON string attribute.
do
  local space = hdf5.create_space("scalar")
  local dtype = hdf5.c_s1:copy()
  local s = json.encode(args)
  dtype:set_size(#s+1)
  local attr = file:create_attribute("parameters", dtype, space)
  attr:write(ffi.cast("const char *", s), dtype)
  space:close()
  dtype:close()
  attr:close()
end

-- Create observables.
local observables = {}
if args.snapshot then table.insert(observables, nm.snapshot(dom, observables, args.snapshot)) end
if args.thermo then table.insert(observables, nm.thermo(integrate, file, args.thermo)) end
if args.trajd then table.insert(observables, nm.trajd(integrate, file, args.trajd)) end
if args.trajs then table.insert(observables, nm.trajs(integrate, file, args.trajs)) end
if args.species then table.insert(observables, nm.species(integrate, file, args.species)) end
if args.msdd then table.insert(observables, nm.corr(integrate, file, nm.msdd(dom), args.msdd)) end
if args.vafd then table.insert(observables, nm.corr(integrate, file, nm.vafd(dom), args.vafd)) end
if args.vafd_n then table.insert(observables, nm.corr(integrate, file, nm.vafd_n(dom), args.vafd_n)) end
if args.vafd_p then table.insert(observables, nm.corr(integrate, file, nm.vafd_p(dom), args.vafd_p)) end
if args.orient then table.insert(observables, nm.corr(integrate, file, nm.orient(dom), args.orient)) end

-- Periodically log step, total energy, and remaining run-time.
do
  local nsteps = args.nsteps
  local interval = 10000
  local self = {step = 0}

  function self.observe()
    local t1 = syscall.clock_gettime("monotonic")
    while self.step <= nsteps do
      coroutine.yield()
      local en = integrate.compute_en()
      local t2 = syscall.clock_gettime("monotonic")
      local t = (t2.time - t1.time) / interval * (nsteps - self.step)
      local u = "s"
      if t >= 60 then t, u = t/60, "m" end
      if t >= 60 then t, u = t/60, "h" end
      io.write("#", self.step, " ", en.en_tot, " ", ("%.1f"):format(t), u, "\n")
      io.flush()
      self.step = self.step + interval
      t1 = t2
    end
  end

  table.insert(observables, self)
end

-- Run simulation.
nm.observe(integrate, observables)

-- Synchronize output files to disk.
file:flush()
assert((syscall.fsync(ffi.cast("int *", file:get_vfd_handle())[0])))
file:close()

-- Rename H5MD output file after successful simulation.
assert((syscall.rename(args.output..".tmp", args.output)))
-- Remove snapshot file.
if args.snapshot then assert((syscall.unlink(args.snapshot.output))) end
