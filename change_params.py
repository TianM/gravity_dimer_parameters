import argparse
from random import getrandbits
parser = argparse.ArgumentParser()
parser.add_argument('changeVars', nargs='+', type=str, help='var=value')
args = parser.parse_args()
changedLines = args.changeVars
with open("config.lua", 'r') as f_in, open("config.tmp", 'w') as f_out:
 for line in f_in:
   match = False
   if line.startswith("seed = "):
       f_out.write('seed = "'+'0x'+'%0x' % getrandbits(16 * 4) + '"\n')
       match = True
   else:
       for var in changedLines:
           if (line.startswith(var.split("=")[0]) or line.startswith("-- " + var.split("=")[0])):
               if (var.split("=")[1] != "remove"):
                   f_out.write(var+'\n')
                   match = True
       if match == False:
           f_out.write(line)

with open("../production/config.lua", 'r') as f_in, open("../production/config.tmp", 'w') as f_out:
 for line in f_in:
   match = False
   if line.startswith("seed = "):
       f_out.write('seed = "'+'0x'+'%0x' % getrandbits(16 * 4) + '"\n')
       match = True
   else:
       for var in changedLines:
           if (line.startswith(var.split("=")[0]) or line.startswith("-- " + var.split("=")[0])):
               if (var.split("=")[1] != "remove"):
                   f_out.write(var+'\n')
                   match = True
       if match == False:
           f_out.write(line)
