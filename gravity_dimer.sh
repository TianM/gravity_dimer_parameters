#!/bin/bash -l
#PBS -l nodes=1:ppn=20
#PBS -l walltime=01:30:00


JOB_ID=$(echo $PBS_JOBID | awk -F. '{print $1}')
MYDIR=$(pwd)

. ~/files/setup_gcc_2015.sh

cd $VSC_SCRATCH_NODE

tar zxf /user/leuven/318/vsc31827/files/pc-opt.tgz

. opt/scratch_env.sh
eval $(luarocks path)

git clone https://bitbucket.org/TianM/nano-dimer.git
git clone https://bitbucket.org/TianM/gravity_dimer_parameters.git

scp -r gravity_dimer_parameters/sedimentation_wall/* nano-dimer/examples/gravity_dimer/

cd nano-dimer
. examples/env.sh

cd examples/gravity_dimer/equilibration
python /$MYDIR/change_params.py 'reaction={rate = 1, wall = {reactive = {false, true, false}, dz = 1}}' 'gravc=0.001'
mv -f config.tmp config.lua
mv -f ../production/config.tmp ../production/config.lua

luajit gravity_dimer.lua config.lua

cd ../production
scp config.lua $VSC_SCRATCH/config_${JOB_ID}.lua
luajit gravity_dimer.lua config.lua

mv gravity_dimer.h5 $VSC_SCRATCH/gravity_dimer_${JOB_ID}.h5
